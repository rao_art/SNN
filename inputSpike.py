#Simulate input spike to observe response of Neuron to the input spike

import random

class Spike:

    def __init__(self, num_points=None, type = None, max_value=None, custom_input=None):

        self.type = type
        self.output = custom_input
        if custom_input is not None:
            self.N = len(custom_input)
            self.max_value = max(custom_input)
        else:
            self.N = num_points
            self.max_value = max_value

    def generate_spike(self):

        if self.output is not None:
            return self.output
        else:
            if self.type=='constant':
                # For constant input of max_value
                return [self.max_value] * self.N

            if self.type=='gaussian':
                return [random.random()*self.max_value for i in range(self.N)]



if __name__ == '__main__':
    main()
