# Code to simulate Izhikevich Spiking Neuron in Python from
#    'SIMPLE MODEL OF SPIKING NEURON' by Eugene M. Izhikevich


import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg')               # plot not displayed on using plt.plot
# from scipy.integrate import odeint


class IzhikevichNeuron:
    def __init__(self, a, b, c, d, name='Izhikevich'):
        self.name = name
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.v0 = -65                # initial value of membrane voltage in mV
        self.u0 = self.b * self.v0   # initial value of recovery variable
        self.sim_timestep = 0.1

    '''def model_int(self, x, t):
        v, u = x[0], x[1]
        I = 10              # magnitude of DC input current
        if v >= 30:
            v = self.c
            u = u + self.d

        dvdt = 0.04*v*v + 5*v + 140 - u + I         # cortical dynamics for v in mV scale and t in ms scale
        dudt = self.a * (self.b*v - u)
        return [dvdt, dudt]'''

    def model(self, num_msec):
        N = int(num_msec/self.sim_timestep)
        v_trace = []   # array for solved values of v
        u_trace = []
        v = self.v0
        u = self.u0
        I = 10              # magnitude of DC current
        for i in range(N):
            if v >= 30:     # apex voltage of the spiking Nueron
                v = self.c
                u = u + self.d

            # cortical dynamics for v in mV scale and t in ms scale
            v += self.sim_timestep*(0.04*v*v + 5*v + 140 - u + I)
            u += self.sim_timestep*(self.a * (self.b*v - u))
            v_trace.append(v)
            u_trace.append(u)
        return [v_trace, u_trace]

    def membrane_voltage(self, num_msec):
        # v = odeint(self.model, [self.v0, self.u0], t)
        v = self.model(num_msec)
        self.plot_response(v[0])

    def plot_response(self, v):
        t = list(map((lambda x: x * self.sim_timestep),
                     list(range(len(v)))))   # Some elements have floating point errors
        plt.xlabel('Time in ms')
        plt.ylabel('Membrane Voltage in mV')
        plt.title('Time Response of {}  Neuron \n '
                  'a={}, b={}, c={}, d={}, v0={}, u0={}'.format(self.name, self.a, self.b,
                                                                self.c, self.d, self.v0, self.u0))
        plt.plot(t, v)
        plt.savefig("Images/" + self.name)
        plt.close()


def main():
    # spike = inputSpike.Spike(10)
    sim_time = 150
    thalamoCortical = IzhikevichNeuron(0.02, 0.25, -65, 0.05, 'Thalamocortical') # input parameters which define a thalamocortical neuron
    thalamoCortical.membrane_voltage(sim_time)

    lowThresholdSpiking = IzhikevichNeuron(0.02, 0.25, -65, 2, 'Low Threshold Spiking')
    lowThresholdSpiking.membrane_voltage(sim_time)

    fastSpiking = IzhikevichNeuron(0.1, 0.2, -65, 2, 'Fast Spiking')
    fastSpiking.membrane_voltage(sim_time)

    regularSpiking = IzhikevichNeuron(0.02, 0.2, -65, 8, 'Regular Spiking')
    regularSpiking.membrane_voltage(sim_time)

    intrinsicallyBursting = IzhikevichNeuron(0.02, 0.2, -55, 4, 'Intrinsically Bursting')
    intrinsicallyBursting.membrane_voltage(sim_time)

    chattering = IzhikevichNeuron(0.02, 0.2, -50, 2, 'Chattering')
    chattering.membrane_voltage(sim_time)

    resonator = IzhikevichNeuron(0.1, 0.26, -65, 2, 'Resonater')
    resonator.membrane_voltage(sim_time)

if __name__ == '__main__':
    main()
