<h1> Spiking Neural Networks </h1>

This Repo Contains different types of Neurons for Spiking Neural Networks:
<ol>
<li>Izhikevich Neuron</li>
<li>Leaky Integrate and Fire Neuron</li>
</ol>

The input spike is modelled using the Spike object from the inputSpike.py file 