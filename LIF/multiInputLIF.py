import numpy as np
import LIFNeuron
import random
import sys
sys.path.append('../inputSpike.py')
from inputSpike import Spike

def main():
    sim_time = 100              # number of ms of simulation
    num_inputs = 4              # number of presynaptic inputs

    reciever_neuron = LIFNeuron.LIFNeuron(name='Multi InputLIF')
    N = int(sim_time / reciever_neuron.sim_timestep)  # number of data points in the input train

    weights = dict()            # dictionary of weight values for presynaptic signals
    for i in range(num_inputs):
        weights['w'+str(i+1)] = np.random.rand()


    pre_syn_sig = dict()    # dictionary of presynaptic signals

    for i in range(num_inputs):
        pre_syn_sig['s'+str(i+1)] = Spike(N, 'gaussian',
                                          random.randint(0,50)
                                          ).generate_spike()      #simulation of pre-synaptic inputs


    post_syn_input = np.zeros(N)

    for i in range(num_inputs):
        post_syn_input = post_syn_input + np.array([weights['w'+str(i+1)]*signal for signal in pre_syn_sig['s'+str(i+1)]])

    reciever_neuron.membrane_voltage(sim_time, Spike(type='post_synaptic',
                                                     custom_input=post_syn_input)) #  plot characterstics of the neuron recieving the spike






if __name__=='__main__':
    main()

