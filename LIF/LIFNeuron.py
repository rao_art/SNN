# Code to simulate Leaky Integrate and Fire Neuron in Python
# Based on 'The Leaky Integrate and Fire Neuron Model'  Emin Orhan
import sys
import matplotlib.pyplot as plt
sys.path.append('../inputSpike.py')
from inputSpike import Spike


class LIFNeuron:
    def __init__(self, tau_m=10, vr=0, vth=15, sim_timestep=0.1, v0=0, name='LIF'):
        self.tau_m = tau_m   # membrane time constant in ms
        self.vr = vr     # reset potential in mv
        self.vth = vth   # Threshold potential in mv
        self.name = name
        self.sim_timestep = sim_timestep  # in ms
        self.v0 = v0

    def model(self, num_msec, input_train):
        v_trace = []  # array for solved values of v
        N = int(num_msec/self.sim_timestep)
        self.input_type = input_train.type
        I = input_train.generate_spike()
        v = self.v0
        R = 1  # in mOhm
        for i in range(N):
            if v >= self.vth:     # apex voltage of the spiking Nueron
                v = self.vr
            # cortical dynamics for v in mV scale and t in ms scale
            v += self.sim_timestep*(-v + I[i]*R)/self.tau_m
            v_trace.append(v)
        return v_trace

    def membrane_voltage(self, num_msec, input_train):
        v = self.model(num_msec, input_train)
        self.plot_response(v)

    def plot_response(self, v):
        t = list(map((lambda x: x * self.sim_timestep),
                     list(range(len(v)))))   # Some elements have floating point errors
        plt.xlabel('Time in ms')
        plt.ylabel('Membrane Voltage in mV')
        plt.title('Time Response of {}  Neuron to a {} input\n '.format(self.name, self.input_type))
        plt.plot(t, v)
        plt.savefig("Images/" + self.name + '_' + self.input_type + ' Input')
        plt.close()


def main():
    sim_time = 100
    LIF_Neuron = LIFNeuron()
    N = int(sim_time/LIF_Neuron.sim_timestep) # number of data points in the input train
    forcing_input = Spike(N, 'gaussian', 30)  # this is the forcing function to the differential equation
    LIF_Neuron.membrane_voltage(sim_time, forcing_input)


if __name__ == '__main__':
    main()
